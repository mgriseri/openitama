# Openitama

> *Onitama* in 🦀 for 🕸️.

# Dev

Rust side in `./`:

```
$ cargo doc --no-deps                # Builds doc
$ wasm-pack build crates/onitama-web # Builds wasm module
```

Web side in `./www`:

```
$ npm i     # Installs web deps
$ npm start # Starts Webpack dev server
$ npm build # Builds web app
```
