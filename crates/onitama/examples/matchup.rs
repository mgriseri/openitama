use onitama::ai::*;

fn main() {
    let depth = 2;
    let games = 10;

    println!("Minmax (depth: {}, games: {})", depth, games);

    let eval1 = SimpleEval::new([50, 40, 30, 20, 10, 4, 3, 2, 1, 1]);
    let eval2 = SimpleEval::new([5, 4, 3, 2, 1, 40, 30, 20, 10, 1]);
    let minmax1 = MinMax::new(eval1, depth);
    let minmax2 = MinMax::new(eval2, depth);
    let mut matchup = MatchUp::new(minmax1, minmax2, games);

    dbg!(matchup.run());
}
