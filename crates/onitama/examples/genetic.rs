use onitama::ai::*;
use onitama::genetic::*;

const _SEED: u64 = 12345;

fn main() {
    let mut genetic = Genetic::<SimpleEval, 3, 2, 2, 10, 10>::from_entropy(0.1, 0.1);
    genetic.run();

    let solutions = genetic.sort();
    dbg!(solutions[0]);
}
