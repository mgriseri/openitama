use crate::*;
use ai::*;

use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::Rng;
use rand::SeedableRng;
use std::io::stdout;
use std::io::Write;
use std::time::Instant;

#[derive(Clone, Debug)]
pub struct Genetic<
    T: Eval,
    const POW: u8,
    const POOLS: u8,
    const DEPTH: u8,
    const GAMES: u8,
    const GENERATIONS: u8,
> {
    rng:                  SmallRng,
    n:                    usize,
    len:                  usize,
    individual_mutations: f64,
    gene_mutations:       f64,
    population:           Vec<T>,
}

impl<
        T: Eval,
        const POW: u8,
        const POOLS: u8,
        const DEPTH: u8,
        const GAMES: u8,
        const GENERATIONS: u8,
    > Genetic<T, POW, POOLS, DEPTH, GAMES, GENERATIONS>
{
    pub fn with_seed(individual_mutations: f64, gene_mutations: f64, seed: u64) -> Self {
        Self::new(
            individual_mutations,
            gene_mutations,
            SmallRng::seed_from_u64(seed),
        )
    }

    pub fn from_entropy(individual_mutations: f64, gene_mutations: f64) -> Self {
        Self::new(
            individual_mutations,
            gene_mutations,
            SmallRng::from_entropy(),
        )
    }

    fn new(individual_mutations: f64, gene_mutations: f64, mut rng: SmallRng) -> Self {
        assert!(POOLS.is_power_of_two());

        let n = 2_usize.pow(POW as u32);
        let n = if n >= 4 { n } else { 4 };
        let len = n * POOLS as usize;
        let mut population = Vec::with_capacity(len);

        for _ in 0..len {
            population.push(T::with_rng(&mut rng));
        }

        Self {
            rng,
            n,
            len,
            individual_mutations,
            gene_mutations,
            population,
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn run(&mut self) {
        println!("Running:");
        println!("> POW: {}", POW);
        println!("> POOLS: {}", POOLS);
        println!("> len: {}", self.len());
        println!("> DEPTH: {}", DEPTH);
        println!("> GAMES: {}", GAMES);
        println!("> GENERATIONS: {}", GENERATIONS);
        println!("> individual_mutations: {}", self.individual_mutations);
        println!("> gene_mutations: {}", self.gene_mutations);

        let start = Instant::now();

        for generation in 0..GENERATIONS {
            print!("* Generation {}", generation + 1);
            stdout().flush();

            let start = Instant::now();
            let rng = &mut self.rng;
            let pools = {
                self.population.shuffle(rng);

                let mut pools = vec![];
                let mut population = &mut self.population[..];

                for _ in 0..POOLS {
                    let (p, pop) = population.split_at_mut(self.n as usize);
                    pools.push(Pool::<_, DEPTH, GAMES>::new(
                        p,
                        self.individual_mutations,
                        self.gene_mutations,
                    ));
                    population = pop;
                }

                pools
            };

            for mut pool in pools {
                pool.selection(rng);
                pool.cross_over(rng);
                pool.mutations(rng);
            }

            println!(" (took: {:?})", start.elapsed());
        }

        println!("Done (in {:?})", start.elapsed());
    }

    pub fn sort(mut self) -> Vec<T> {
        println!("Sorting:");

        let start = Instant::now();
        let mut population = self.population;
        let mut len = population.len();

        while len >= 2 {
            let mut pool = Pool::<_, DEPTH, GAMES>::new(
                &mut population[..len],
                self.individual_mutations,
                self.gene_mutations,
            );
            pool.selection(&mut self.rng);
            len /= 2;
        }

        println!("Done (in {:?})", start.elapsed());

        population
    }
}

#[derive(Debug)]
struct Pool<'a, T: Eval, const DEPTH: u8, const GAMES: u8> {
    individual_mutations: f64,
    gene_mutations:       f64,
    population:           &'a mut [T],
}

impl<'a, T: Eval, const DEPTH: u8, const GAMES: u8> Pool<'a, T, DEPTH, GAMES> {
    pub fn new(population: &'a mut [T], individual_mutations: f64, gene_mutations: f64) -> Self {
        assert!(population.len().is_power_of_two());
        assert!(population.len() >= 2);

        Self {
            individual_mutations,
            gene_mutations,
            population,
        }
    }

    pub fn selection<R: Rng>(&mut self, rng: &mut R) {
        let half = self.population.len() / 2;

        for i in 0..half {
            let red = self.population[i];
            let blue = self.population[i + half];
            let red = MinMax::new(red, DEPTH as usize);
            let blue = MinMax::new(blue, DEPTH as usize);

            let mut matchup = MatchUp::with_seed(red, blue, GAMES as usize, rng.next_u64());
            let winner = match matchup.run().0 {
                Some(player) => player,
                None =>
                    if rng.gen() {
                        Red
                    } else {
                        Blue
                    },
            };

            if winner == Blue {
                self.population.swap(i, i + half);
            }
        }
    }

    pub fn cross_over<R: Rng>(&mut self, rng: &mut R) {
        let half = self.population.len() / 2;

        for i in (0..half).step_by(2) {
            let father = self.population[i];
            let mother = self.population[i + 1];
            let [boy, girl] = CrossOver::uniform_with_rng([father, mother], rng);

            self.population[half + i] = boy;
            self.population[half + i + 1] = girl;
        }
    }

    pub fn mutations<R: Rng>(&mut self, rng: &mut R) {
        let len = self.population.len();
        let half = len / 2;

        for i in half..len {
            if rng.gen_bool(self.individual_mutations) {
                for byte in self.population[i].as_mut() {
                    if rng.gen_bool(self.gene_mutations) {
                        *byte ^= 1 << rng.gen_range(0..u8::BITS);
                    }
                }
            }
        }
    }
}
