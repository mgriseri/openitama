use super::*;
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::SeedableRng;

/// Trait for AIs.
pub trait AI: Clone {
    fn play(&mut self, game: Game) -> Play;
}

/// A retard [`AI`].
///
/// Chooses first legal move.
#[derive(Copy, Clone, Debug)]
pub struct Retard;

impl AI for Retard {
    fn play(&mut self, game: Game) -> Play {
        game.plays()[0]
    }
}

/// A random [`AI`].
///
/// Chooses random legal move.
#[derive(Clone, Debug)]
pub struct Random(SmallRng);

impl Random {
    pub fn new() -> Self {
        Self(SmallRng::from_entropy())
    }

    pub fn with_seed(seed: u64) -> Self {
        Self(SmallRng::seed_from_u64(seed))
    }
}

impl AI for Random {
    fn play(&mut self, game: Game) -> Play {
        *game.plays().choose(&mut self.0).unwrap()
    }
}
