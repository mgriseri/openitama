use super::*;
use rand::rngs::SmallRng;
use rand::Rng;
use rand::SeedableRng;

pub trait Eval: Copy + AsRef<[u8]> + AsMut<[u8]> + Default {
    fn with_rng<T: Rng>(rng: &mut T) -> Self;

    fn eval_player(&self, game: Game, player: Player) -> f32;

    fn rand() -> Self {
        Self::with_rng(&mut SmallRng::from_entropy())
    }

    fn with_seed(seed: u64) -> Self {
        Self::with_rng(&mut SmallRng::seed_from_u64(seed))
    }

    fn eval(&self, game: Game) -> f32 {
        debug_assert!(matches!(game.state(), State::Turn(_)));

        self.eval_player(game, Red) - self.eval_player(game, Blue)
    }

    fn len(&self) -> usize {
        self.as_ref().len()
    }
}

// pieces:   SIZE
// distance: SIZE - 1
// moves:    u8
#[derive(Copy, Clone, Default, Debug)]
pub struct SimpleEval([u8; Self::LEN]);

impl SimpleEval {
    const LEN: usize = 2 * SIZE;

    pub fn new(bytes: [u8; Self::LEN]) -> Self {
        Self(bytes)
    }

    pub fn rand() -> Self {
        let mut bytes = [0; Self::LEN];
        let mut rng = SmallRng::from_entropy();
        rng.fill(&mut bytes);

        Self(bytes)
    }

    pub fn pieces(&self) -> &[u8] {
        &self.0[0..SIZE]
    }

    pub fn distance(&self) -> &[u8] {
        &self.0[SIZE..Self::LEN - 1]
    }

    pub fn moves(&self) -> u8 {
        self.0[Self::LEN - 1]
    }
}

impl AsRef<[u8]> for SimpleEval {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl AsMut<[u8]> for SimpleEval {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self.0
    }
}

impl Eval for SimpleEval {
    fn with_rng<T: Rng>(rng: &mut T) -> Self {
        let mut bytes = [0; Self::LEN];
        rng.fill(&mut bytes);

        Self(bytes)
    }

    fn eval_player(&self, game: Game, player: Player) -> f32 {
        let pieces = game.pieces(player).count() - 1;
        let distance = game.distance(player) as usize - 1;
        let moves = game.plays().len() as u8;

        (self.pieces()[pieces] + self.distance()[distance] + self.moves() * moves) as _
    }
}
