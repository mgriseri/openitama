use super::*;
use rand::rngs::SmallRng;
use rand::RngCore;
use rand::SeedableRng;
use std::cmp::Ordering;
use std::time::Duration;
use std::time::Instant;

/// A match-up between two [`AI`]s.
#[derive(Debug)]
pub struct MatchUp<R: AI, B: AI> {
    rng:   SmallRng,
    games: usize,
    red:   R,
    blue:  B,
}

impl<R: AI, B: AI> MatchUp<R, B> {
    pub fn new(red: R, blue: B, games: usize) -> Self {
        Self {
            rng: SmallRng::from_entropy(),
            red,
            blue,
            games,
        }
    }

    pub fn with_seed(red: R, blue: B, games: usize, seed: u64) -> Self {
        Self {
            rng: SmallRng::seed_from_u64(seed),
            red,
            blue,
            games,
        }
    }

    pub fn run(
        &mut self,
    ) -> (
        Option<Player>,
        (usize, usize, usize),
        (f32, f32, f32),
        Duration,
    ) {
        #[rustfmt::skip]
        macro_rules! brk { ($x:ident) => {{ $x += 1; break; }} }

        let mut games = self.games;
        let mut red = 0;
        let mut blue = 0;
        let mut draw = 0;

        let now = Instant::now();

        while games != 0 {
            let mut game = Game::rand_with_seed(self.rng.next_u64());
            games -= 1;

            loop {
                game.play_mut(match game.state() {
                    State::Turn(Red) => self.red.play(game),
                    State::Turn(Blue) => self.blue.play(game),
                    State::Won(Red) => brk!(red),
                    State::Won(Blue) => brk!(blue),
                    State::Draw => brk!(draw),
                });
            }
        }

        let elapsed = now.elapsed();
        let redpc = red as f32 * 100. / self.games as f32;
        let bluepc = blue as f32 * 100. / self.games as f32;
        let drawpc = draw as f32 * 100. / self.games as f32;
        let winner = match red.cmp(&blue) {
            Ordering::Greater => Some(Red),
            Ordering::Less => Some(Blue),
            Ordering::Equal => None,
        };

        (winner, (red, blue, draw), (redpc, bluepc, drawpc), elapsed)
    }
}
