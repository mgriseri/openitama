use super::*;

#[derive(Copy, Clone, Debug)]
pub struct MinMax<T: Eval> {
    eval:  T,
    depth: usize,
}

impl<T: Eval> MinMax<T> {
    pub fn new(eval: T, depth: usize) -> Self {
        Self { eval, depth }
    }
}

impl<T: Eval> MinMax<T> {
    pub fn minmax(&self, game: Game, depth: usize) -> f32 {
        let (mut value, f): (_, fn(_, _) -> _) = match game.state() {
            State::Turn(Red) => (f32::NEG_INFINITY, f32::max),
            State::Turn(Blue) => (f32::INFINITY, f32::min),
            State::Won(Red) => return f32::INFINITY,
            State::Won(Blue) => return f32::NEG_INFINITY,
            State::Draw => return 0.,
        };

        if depth == 0 {
            self.eval.eval(game)
        } else {
            for play in game.plays() {
                value = f(value, self.minmax(game.play(play), depth - 1));
            }

            value
        }
    }
}

impl<T: Eval> AI for MinMax<T> {
    fn play(&mut self, game: Game) -> Play {
        let f: fn(_, _) -> _ = match game.state() {
            State::Turn(Red) => |a, b| a > b,
            State::Turn(Blue) => |a, b| a < b,
            _ => unreachable!("Game ended already!"),
        };

        let mut plays = game.plays().into_iter();
        let mut best_play = plays.next().expect("No plays for this game!");
        let mut best_score = self.minmax(game.play(best_play), self.depth);

        for play in plays {
            let score = self.minmax(game.play(play), self.depth);

            if f(score, best_score) {
                best_play = play;
                best_score = score;
            }
        }

        best_play
    }
}
