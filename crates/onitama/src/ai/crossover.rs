use super::*;
use rand::rngs::SmallRng;
use rand::Rng;
use rand::SeedableRng;

#[derive(Copy, Clone, Debug)]
pub struct CrossOver;

impl CrossOver {
    pub fn uniform<T: Eval>([father, mother]: [T; 2]) -> [T; 2] {
        Self::uniform_with_rng([father, mother], &mut SmallRng::from_entropy())
    }

    pub fn uniform_with_seed<T: Eval>([father, mother]: [T; 2], seed: u64) -> [T; 2] {
        Self::uniform_with_rng([father, mother], &mut SmallRng::seed_from_u64(seed))
    }

    pub fn uniform_with_rng<T: Eval, R: Rng>([father, mother]: [T; 2], rng: &mut R) -> [T; 2] {
        let mut boy = T::default();
        let mut girl = T::default();

        for i in 0..father.len() {
            if rng.gen() {
                boy.as_mut()[i] = father.as_ref()[i];
                girl.as_mut()[i] = mother.as_ref()[i];
            } else {
                boy.as_mut()[i] = mother.as_ref()[i];
                girl.as_mut()[i] = father.as_ref()[i];
            }
        }

        [boy, girl]
    }

    pub fn linear<T: Eval>([father, mother]: [T; 2], alpha: f32) -> T {
        debug_assert!(0. < alpha && alpha < 1.);
        let mut child = T::default();

        for i in 0..child.len() {
            let father = father.as_ref()[i] as f32;
            let mother = mother.as_ref()[i] as f32;
            child.as_mut()[i] = (alpha * father + (1. - alpha) * mother).round() as u8;
        }

        child
    }

    fn single_point<T: Eval>([father, mother]: [T; 2], i: usize) -> [T; 2] {
        debug_assert!(0 < i && i < father.len() - 1);
        let mut boy = mother.clone();
        let mut girl = father.clone();

        boy.as_mut()[..i].copy_from_slice(&father.as_ref()[..i]);
        girl.as_mut()[..i].copy_from_slice(&mother.as_ref()[..i]);

        [boy, girl]
    }

    fn two_point<T: Eval>([father, mother]: [T; 2], i: usize, j: usize) -> [T; 2] {
        debug_assert!(0 < i && i < j && j < father.len() - 1);
        let mut boy = father.clone();
        let mut girl = mother.clone();

        boy.as_mut()[i..j].copy_from_slice(&mother.as_ref()[i..j]);
        girl.as_mut()[i..j].copy_from_slice(&father.as_ref()[i..j]);

        [boy, girl]
    }
}
