use super::*;
use rand::rngs::SmallRng;
use rand::seq::IteratorRandom;
use rand::seq::SliceRandom;
use rand::SeedableRng;
use std::ops::Index;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Side {
    pub pieces: [Option<Square>; SIZE],
    pub cards:  [usize; HAND],
}

impl Side {
    pub fn new(player: Player, cards: [usize; HAND]) -> Self {
        macro_rules! pieces {
            ($rank:ident $($file:ident)*) => { [$(Some(Square($file, $rank))),*] };
        }

        let rank = player.rank();
        Self {
            pieces: pieces!(rank A B C D E),
            cards,
        }
    }

    pub fn pieces(&self) -> impl '_ + Iterator<Item = (Piece, Square)> {
        self.pieces
            .into_iter()
            .enumerate()
            .filter_map(|(i, square)| {
                if let Some(square) = square {
                    Some((i, square))
                } else {
                    None
                }
            })
            .map(move |(i, square)| (Piece::from(i), square))
    }

    pub fn squares(&self) -> impl '_ + Iterator<Item = Square> {
        self.pieces.into_iter().filter_map(|square| square)
    }

    pub fn cards(&self) -> [Card; HAND] {
        [CARDS[self.cards[0]], CARDS[self.cards[1]]]
    }

    pub fn square(&self, piece: Piece) -> &Option<Square> {
        &self.pieces[piece.index()]
    }

    pub fn square_mut(&mut self, piece: Piece) -> &mut Option<Square> {
        &mut self.pieces[piece.index()]
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Play {
    Card {
        card: usize,
        src:  Square,
        dest: Square,
    },
    Discard(usize),
}

impl Play {
    pub fn card(&self) -> Card {
        CARDS[match *self {
            Play::Card { card, .. } => card,
            Play::Discard(card) => card,
        }]
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum State {
    Turn(Player),
    Won(Player),
    Draw,
}

impl State {
    pub fn turn(&self) -> Option<Player> {
        if let Self::Turn(player) = *self {
            Some(player)
        } else {
            None
        }
    }

    pub fn winner(&self) -> Option<Player> {
        if let Self::Won(player) = *self {
            Some(player)
        } else {
            None
        }
    }

    pub fn is_playing(&self) -> bool {
        self.turn().is_some()
    }

    pub fn is_draw(&self) -> bool {
        if let Self::Draw = *self {
            true
        } else {
            false
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Game {
    state: State,
    board: Board,
    red:   Side,
    blue:  Side,
    spare: usize,
    timer: u8,
}

impl Game {
    const DRAW_LIMIT: u8 = 40;

    pub fn new(red: [usize; HAND], blue: [usize; HAND], spare: usize) -> Self {
        Self {
            state: State::Turn(CARDS[spare].stamp),
            board: Board::default(),
            red: Side::new(Red, red),
            blue: Side::new(Blue, blue),
            spare,
            timer: Self::DRAW_LIMIT,
        }
    }

    pub fn rand() -> Self {
        Self::rand_with(SmallRng::from_entropy())
    }

    pub fn rand_with_seed(seed: u64) -> Self {
        Self::rand_with(SmallRng::seed_from_u64(seed))
    }

    pub fn state(&self) -> State {
        self.state
    }

    pub fn player(&self) -> Option<Player> {
        match self.state {
            State::Turn(player) => Some(player),
            _ => None,
        }
    }

    pub fn side(&self, player: Player) -> &Side {
        match player {
            Red => &self.red,
            Blue => &self.blue,
        }
    }

    pub fn spare(&self) -> Card {
        CARDS[self.spare]
    }

    pub fn pieces(&self, player: Player) -> impl '_ + Iterator<Item = (Piece, Square)> {
        self[player].pieces()
    }

    pub fn distance(&self, player: Player) -> u8 {
        let king = self[(player, King)].unwrap();
        let square = Square::king(!player);

        let king = (king.file() as i8, king.rank() as i8);
        let square = (square.file() as i8, square.rank() as i8);

        let distance = ((king.0 - square.0).abs(), (king.1 - square.1).abs());
        distance.0.max(distance.1) as u8
    }

    pub fn plays(&self) -> Vec<Play> {
        let mut plays = vec![];
        let player = self.player().unwrap();
        let flipper = player.flipper();
        let side = self.side(player);
        let cards = side.cards();
        let squares = side.squares();
        let is_valid = move |square| !matches!(self[square], Some((p, _)) if p == player);

        for src in squares {
            for (c, card) in cards.iter().enumerate() {
                for mov in card.moves.iter().map(flipper) {
                    if let Some(dest) = src.apply(mov) {
                        if is_valid(dest) {
                            plays.push(Play::Card { card: c, src, dest });
                        }
                    }
                }
            }
        }

        if plays.is_empty() {
            for card in 0..HAND {
                plays.push(Play::Discard(card));
            }
        }

        plays
    }

    pub fn dests(&self, card: usize, src: Square) -> impl '_ + Iterator<Item = Square> {
        let player = self.player().unwrap();
        let side = self.side(player);
        let card = side.cards()[card];
        let mut moves = card.moves.iter().map(player.flipper());

        let has_piece = move |square| matches!(self[square], Some((p, _)) if p == player);
        debug_assert!(has_piece(src));

        std::iter::from_fn(move || loop {
            if let Some(dest) = src.apply(moves.next()?).filter(|&dest| !has_piece(dest)) {
                return Some(dest);
            }
        })
    }

    pub fn sort(&self, plays: &mut [Play]) {
        use std::cmp::Ordering::*;

        plays.sort_unstable_by(|a, b| match (*a, *b) {
            (Play::Card { dest: dest_a, .. }, Play::Card { dest: dest_b, .. }) => {
                match (self[dest_a], self[dest_b]) {
                    (Some((_, King)), Some((_, King))) => Equal,
                    (Some((_, King)), Some(_)) => Greater,
                    (Some(_), Some((_, King))) => Less,
                    (Some(_), Some(_)) => Equal,
                    (Some(_), None) => Greater,
                    (None, Some(_)) => Less,
                    (None, None) => Equal,
                }
            }
            (Play::Card { .. }, Play::Discard(_)) => Greater,
            (Play::Discard(_), Play::Card { .. }) => Less,
            (Play::Discard(_), Play::Discard(_)) => Equal,
        });
    }

    pub fn play(&self, play: Play) -> Self {
        let mut game = *self;
        game.play_mut(play);
        game
    }

    pub fn play_mut(&mut self, play: Play) -> State {
        let player = self.player().unwrap();

        let (capture, discard) = match play {
            Play::Card { card, src, dest } => {
                let (_, piece) = self[src].unwrap();
                let capture = self[dest];

                // Update board
                self.board[src] = None;
                self.board[dest] = Some((player, piece));

                // Update pieces
                *self.side_mut(player).square_mut(piece) = Some(dest);
                if let Some((_, piece)) = capture {
                    *self.side_mut(!player).square_mut(piece) = None;
                }

                (capture, card)
            }
            Play::Discard(card) => (None, card),
        };

        // Update hand
        std::mem::swap(&mut self.spare, {
            &mut match player {
                Red => &mut self.red,
                Blue => &mut self.blue,
            }
            .cards[discard]
        });

        // Update timer
        if capture.is_none() {
            self.timer -= 1;
        } else {
            self.timer = Self::DRAW_LIMIT;
        }

        // Update state
        let stone = capture == Some((!player, King));
        let stream = self[Square::king(!player)] == Some((player, King));

        self.state = if stone || stream {
            State::Won(player)
        } else if self.timer == 0 {
            State::Draw
        } else {
            State::Turn(!player)
        };

        self.state
    }
}

impl Game {
    pub fn rand_with(mut rng: SmallRng) -> Self {
        let mut cards = [0; HAND * 2 + 1];
        (0..CARDS.len()).choose_multiple_fill(&mut rng, &mut cards);
        cards.shuffle(&mut rng);

        Self::new([cards[0], cards[1]], [cards[2], cards[3]], cards[4])
    }

    fn side_mut(&mut self, player: Player) -> &mut Side {
        match player {
            Red => &mut self.red,
            Blue => &mut self.blue,
        }
    }
}

impl Index<Square> for Game {
    type Output = Option<(Player, Piece)>;

    fn index(&self, square: Square) -> &Option<(Player, Piece)> {
        &self.board[square]
    }
}

impl Index<Player> for Game {
    type Output = Side;

    fn index(&self, player: Player) -> &Side {
        self.side(player)
    }
}

impl Index<(Player, Piece)> for Game {
    type Output = Option<Square>;

    fn index(&self, (player, piece): (Player, Piece)) -> &Option<Square> {
        self.side(player).square(piece)
    }
}
