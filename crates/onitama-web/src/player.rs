use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// ⛹️ A player (**`enum`**).
    ///
    /// An **`enum`** which is either `RED` or `BLUE`.
    #[derive(Copy, Clone)]
    Player
);

variants!(Player {
    RED Red,
    BLUE Blue,
});
