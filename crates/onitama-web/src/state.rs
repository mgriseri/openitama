use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// State of a [`Game`].
    #[derive(Copy, Clone)]
    State
);

getters!(State {
    /// Returns the [`Player`] to play,
    /// or `undefined` if the game has ended.
    turn -> Option<Player> { .map_into() }
    /// Returns the winning [`Player`],
    /// or `undefined` if the game is playing or drawn.
    winner -> Option<Player> { .map_into() }
    /// Returns `true` if the game is playing,
    /// or `false` if the game has ended.
    is_playing -> bool {}
    /// Returns `true` if the game has ended with a draw,
    /// or `false` if the game is playing or won.
    is_draw -> bool {}
});
