//! *Onitama* in 🦀 for 🕸️.
//!
//! # JavaScript bindings
//!
//! The items in this crate are to be used from JavaScript through wasm.
//!
//! ## Classes
//!
//! Structs below map to JS classes, exported as named exports,
//! and may ship with **`Constructor`**, **`Methods`**,
//! **`Getters`**/**`Variants`** and **`Setters`**.
//!
//! Example of an import in JS:
//! ```javascript
//! import { Foo } from 'openitama';
//! ```
//!
//! Example of a **`Constructor`** use in JS:
//! ```javascript
//! const foo = new Foo(...);
//! ```
//!
//! Example of a **`Method`** use in JS:
//! ```javascript
//! const bar = foo.bar();
//! ```
//!
//! Example of a **`Getter`** use in JS:
//! ```javascript
//! const bar = foo.bar;
//! ```
//!
//! Example of a **`Setter`** use in JS:
//! ```javascript
//! foo.bar = bar;
//! ```
//!
//! Our types also have `toString` methods on the JS side (yet no `toJSON`).
//! Call this method to help debug your app,
//! but be aware it outputs the inner Rust representation of the value!
//!
//! ```javascript
//! console.log(foo.toString()); // Not exactly what you think :/
//!                              // Still better than "Object { ptr: 1114248 }" :)
//! ```
//!
//! ## Enums
//!
//! Some types are **`enums`**, which means the value is either one of its
//! variants. We use uppercase **`Getters`** (**`Variants`**) to test for the
//! actual variant.
//!
//! **`Enums`** have **`Constructors`** that take a `String` of
//! the uppercase variant name as parameter.
//!
//! For example, [`Player`] is an **`enum`** with `RED` and `BLUE` variants:
//! ```javascript
//! const player = new Player('RED');
//!
//! if (player.RED) {
//!   // red player
//! } else /* if (player.BLUE) */ {
//!   // blue player
//! }
//! ```
//!
//!
//! ## Option
//!
//! The [`Option`] type in Rust is a type-safe `null` wrapper. It maps to
//! `undefined` in JS.
//!
//! Example: Given the Rust function `fn foo() -> Option<Bar>`, calling `foo` on
//! the JS side should look like:
//! ```javascript
//! const bar = foo();
//!
//! if (bar === undefined) {
//!   // there is no bar!
//! } else {
//!   // use bar here :)
//! }
//! ```

#![allow(unused)]

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

macro_rules! bind {
    (
        $(#[$doc:meta])*
        $Type:ident
    ) => {
        $(#[$doc])*
        #[wasm_bindgen]
        pub struct $Type(onitama::$Type);

        #[doc(hidden)]
        #[wasm_bindgen]
        impl $Type {
            #[wasm_bindgen(js_name = toString)]
            pub fn to_string(&self) -> String {
                format!("{:#?}", self.0)
            }
        }

        impl From<onitama::$Type> for $Type {
            fn from(value: onitama::$Type) -> Self {
                Self(value)
            }
        }

        impl From<$Type> for onitama::$Type {
            fn from(value: $Type) -> Self {
                value.0
            }
        }

        impl MapInto<$Type> for Option<onitama::$Type> {
            fn map_into(self) -> Option<$Type> {
                self.map(Into::into)
            }
        }
    };
}

macro_rules! getters {
    ($Type:ident { $(
        $(#[$doc:meta])*
        $fn:ident -> $ret:ty { $($tt:tt)* }
    )* }) => {
        /// **`Getters`**
        #[wasm_bindgen]
        impl $Type { $(
            $(#[$doc])*
            #[wasm_bindgen(getter)]
            pub fn $fn(&self) -> $ret { self.0.$fn()$($tt)* }
        )* }
    };
}

macro_rules! variants {
    ($Type:ident { $( $fn:ident $Variant:ident, )* }) => {
        /// **`Constructor`**
        #[wasm_bindgen]
        impl $Type {
            #[doc = concat!("Creates a new `", stringify!($Type), "`.")]
            #[doc = ""]
            #[doc = "Accepts:"]
            $(#[doc = concat!("- `'", stringify!($fn), "'`")])*
            #[wasm_bindgen(constructor)]
            pub fn new(string: &str) -> Self {
                match string {
                    $(stringify!($fn) => onitama::$Variant,)*
                    _ => unreachable!(),
                }
                .into()
            }
        }

        /// **`Variants`**
        #[wasm_bindgen]
        #[allow(non_snake_case)]
        impl $Type { $(
            #[doc = concat!("Returns `true` if `", stringify!($fn), "`.")]
            #[wasm_bindgen(getter)]
            pub fn $fn(&self) -> bool { self.0 == onitama::$Variant }
        )* }
    };
}

mod card;
mod game;
mod piece;
mod player;
mod square;
mod state;

use onitama;

pub use card::*;
pub use game::*;
pub use piece::*;
pub use player::*;
pub use square::*;
pub use state::*;

trait MapInto<T> {
    fn map_into(self) -> Option<T>;
}
