use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// ♟️ A piece (**`enum`**).
    #[derive(Copy, Clone)]
    Piece
);

variants!(Piece {
    PAWN_A PawnA,
    PAWN_B PawnB,
    KING King,
    PAWN_D PawnD,
    PAWN_E PawnE,
});
