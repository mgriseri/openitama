use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// A board file (**`enum`**).
    #[derive(Copy, Clone)]
    File
);

variants!(File {
    A A,
    B B,
    C C,
    D D,
    E E,
});

bind!(
    /// A board rank (**`enum`**).
    #[derive(Copy, Clone)]
    Rank
);

variants!(Rank {
    ONE One,
    TWO Two,
    THREE Three,
    FOUR Four,
    FIVE Five,
});

bind!(
    /// ◻️ A board square.
    #[derive(Copy, Clone)]
    Square
);

getters!(Square {
    /// Returns the [`File`] of the square.
    file -> File { .into() }
    /// Returns the [`Rank`] of the square.
    rank -> Rank { .into() }
});
