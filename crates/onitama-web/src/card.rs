use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// 🃏 A card.
    #[derive(Copy, Clone)]
    Card
);
