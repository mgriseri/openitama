use super::*;
use wasm_bindgen::prelude::*;

bind!(
    /// 🎮 A game of *Onitama*.
    #[derive(Copy, Clone)]
    Game
);

/// **`Constructor`**
#[wasm_bindgen]
impl Game {
    /// Creates a new `Game`.
    ///
    /// Takes indexes of [`Card`]s for the red player hand (`r0`, `r1`),
    /// the blue player hand (`b0`, `b1`) and the spare card (`s`).
    ///
    /// See [`onitama::CARDS`]
    /// ([src](../src/onitama/lib.rs.html#26-120)).
    #[wasm_bindgen(constructor)]
    pub fn new(r0: usize, r1: usize, b0: usize, b1: usize, s: usize) -> Self {
        Self(onitama::Game::new([r0, r1], [b0, b1], s))
    }
}

getters!(Game {
    /// Returns the spare [`Card`].
    spare -> Card { .into() }
    /// Returns the [`State`] of the game.
    state -> State { .into() }
});

/// **`Methods`**
#[wasm_bindgen]
impl Game {
    /// Returns the `card`th [`Card`] (either `0` or `1`) in `player`'s hand.
    pub fn get_card(&self, player: &Player, card: usize) -> Card {
        self.0.side((*player).into()).cards()[card].into()
    }

    /// Returns the [`Square`] occupied by `player`'s `piece`,
    /// or `undefined` if it was captured.
    pub fn get_square(&self, player: &Player, piece: &Piece) -> Option<Square> {
        (*self.0.side((*player).into()).square((*piece).into())).map_into()
    }
}
