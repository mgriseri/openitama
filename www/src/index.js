import '@riotjs/hot-reload'
import * as riot from 'riot'
import App from './components/app.riot'

import * as openitama from 'openitama';
import { Game, Piece } from 'openitama';


const mountApp = riot.component(App)
const app = mountApp(document.getElementById('app'))

const game = new openitama.Game(0, 1, 2, 3, 4);
const player = game.state.turn;
console.log({ game });
console.log(game.toString());
console.log(player);
console.log(game.get_card(player, 0));
console.log(player);
console.log(game.get_card(player, 1));

const square = game.get_square(player, new Piece('PAWN_A'));
console.log(square);
console.log(square.file);
console.log(square.rank.toString());
