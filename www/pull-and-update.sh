#!/bin/bash

dir=$(dirname "${BASH_SOURCE[0]}")
cd $dir/..

git pull &&
cargo doc --no-deps &&
wasm-pack build crates/onitama-web
